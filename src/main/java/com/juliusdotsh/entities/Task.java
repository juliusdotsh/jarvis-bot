package com.juliusdotsh.entities;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class Task {
    private String text, type;
    private Boolean completed, isDue;
    private Date date;
}
