package com.juliusdotsh.commands;

import com.juliusdotsh.components.LanguageManager;
import com.juliusdotsh.entities.User;
import com.juliusdotsh.interfaces.Command;
import com.juliusdotsh.repositories.UserRepository;
import com.juliusdotsh.services.JarvisBotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Message;

import javax.transaction.Transactional;

@Component
public class ChangeLanguage implements Command {

    private UserRepository userRepository;
    private Settings settings;
    private JarvisBotService jarvisBotService;

    @Autowired
    public ChangeLanguage(UserRepository userRepository, Settings settings, JarvisBotService jarvisBotService) {
        this.userRepository = userRepository;
        this.settings = settings;
        this.jarvisBotService = jarvisBotService;
    }

    @Override
    @Transactional
    public void onExec(Message message) {
        String text = message.getText();
        User user = userRepository.findByTelegramId(message.getChatId());
        if (LanguageManager.getLabel(user, "language.en").equals(text)) {
            user.setLanguage("en");
            user.setState(0);
            userRepository.save(user);
            settings.onExec(message);
        } else if (LanguageManager.getLabel(user, "language.it").equals(text)) {
            user.setLanguage("it");
            user.setState(0);
            userRepository.save(user);
            settings.onExec(message);
        } else {
            jarvisBotService.sendMessage(user.getTelegramId(), LanguageManager.getLabel(user, "error.language.not.found"));
        }
    }
}
