package com.juliusdotsh.commands;

import com.juliusdotsh.components.LanguageManager;
import com.juliusdotsh.entities.User;
import com.juliusdotsh.interfaces.Command;
import com.juliusdotsh.repositories.UserRepository;
import com.juliusdotsh.services.impl.JarvisBotServiceImpl;
import com.juliusdotsh.utility.KeyboardManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

import javax.transaction.Transactional;

@Component
public class EnableCron implements Command {

	private UserRepository userRepository;
	private JarvisBotServiceImpl jarvisBotService;

	@Autowired
	public EnableCron(UserRepository userRepository, JarvisBotServiceImpl jarvisBotService) {
		this.userRepository = userRepository;
		this.jarvisBotService = jarvisBotService;
	}


	@Override
	@Transactional
	public void onExec(Message message) {
		User good = userRepository.findByTelegramId(message.getChatId());
		good.setCron(true);
		userRepository.save(good);
		SendMessage sendMessage = new SendMessage()
                .setReplyMarkup(KeyboardManager.getSettingsKeyboard(good))
				.enableMarkdown(true)
				.setChatId(good.getTelegramId())
                .setText(LanguageManager.getLabel(good, "message.cron.enabling"));
		jarvisBotService.execute(sendMessage);
	}
}
