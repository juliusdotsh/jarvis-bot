package com.juliusdotsh.commands;

import com.juliusdotsh.components.LanguageManager;
import com.juliusdotsh.entities.User;
import com.juliusdotsh.interfaces.Command;
import com.juliusdotsh.repositories.UserRepository;
import com.juliusdotsh.services.JarvisBotService;
import com.juliusdotsh.utility.KeyboardManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

import javax.transaction.Transactional;

@Component
public class Settings implements Command {

	private UserRepository userRepository;
	private JarvisBotService jarvisBotService;

	@Autowired
	public Settings(UserRepository userRepository, JarvisBotService jarvisBotService) {
		this.userRepository = userRepository;
		this.jarvisBotService = jarvisBotService;
	}

	@Override
	@Transactional
	public void onExec(Message message) {
		User user = userRepository.findByTelegramId(message.getChatId());
		if (user.getState() != 0) {
			user.setState(0);
			userRepository.save(user);
		}
		SendMessage sendMessage = new SendMessage()
				.setReplyMarkup(KeyboardManager.getSettingsKeyboard(user))
				.enableMarkdown(true)
				.setChatId(user.getTelegramId())
				.setText(LanguageManager.getLabel(user, "message.command.settings"));
		jarvisBotService.execute(sendMessage);
	}

}
