package com.juliusdotsh.services;

import com.juliusdotsh.entities.User;
import org.telegram.telegrambots.meta.api.interfaces.BotApiObject;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Message;

public interface JarvisBotService {
	void onUpdateReceived(Message message);

    Message sendMessageHome(long chatId, String text, User user);

    Message sendMessage(long chatId, String text);

	BotApiObject execute(BotApiMethod botApiMethod);

    void cronDailyTasks();
}
