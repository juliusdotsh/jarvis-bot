package com.juliusdotsh.services.impl;

import com.juliusdotsh.bot.JarvisBot;
import com.juliusdotsh.commands.DailyTasks;
import com.juliusdotsh.components.Habitica;
import com.juliusdotsh.components.LanguageManager;
import com.juliusdotsh.components.StateHandler;
import com.juliusdotsh.entities.User;
import com.juliusdotsh.interfaces.Command;
import com.juliusdotsh.repositories.UserRepository;
import com.juliusdotsh.services.JarvisBotService;
import com.juliusdotsh.utility.KeyboardManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Calendar;
import java.util.List;

@Service
public class JarvisBotServiceImpl implements JarvisBotService {

	private Logger log = LoggerFactory.getLogger(JarvisBotServiceImpl.class);

	private JarvisBot jarvisBot;
	private UserRepository userRepository;
	private Habitica habitica;
	private StateHandler stateHandler;
	private DailyTasks dailyTask;

	@Lazy
	@Autowired
	public JarvisBotServiceImpl(JarvisBot jarvisBot, UserRepository userRepository, Habitica habitica, StateHandler stateHandler, DailyTasks dailyTask) {
		this.jarvisBot = jarvisBot;
		this.userRepository = userRepository;
		this.habitica = habitica;
		this.stateHandler = stateHandler;
		this.dailyTask = dailyTask;
	}

	@Override
	public void onUpdateReceived(Message message){
		Command command = stateHandler.findCommandByState(message);
		User user = userRepository.findByTelegramId(message.getChatId());
		if (command == null) {
			sendMessage(message.getChatId(), LanguageManager.getLabel(user, "error.command.not.valid"));
		} else {
			command.onExec(message);
		}
	}

	@Override
	public Message sendMessageHome(long chatId, String text, User user) {
		return sendMessage(chatId, text, KeyboardManager.getHomeKeyboard(user));
	}

	@Override
	public Message sendMessage(long chatId, String text){
		SendMessage sendMessage = new SendMessage()
				.enableMarkdown(true)
				.setChatId(chatId)
				.setText(text);
		return execute(sendMessage);
	}

	@Override
	public Message execute(BotApiMethod method) {
		try {
			return (Message) jarvisBot.execute(method);

		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void cronDailyTasks(){
		log.info(String.format("Runned Chron Job at %s", Calendar.getInstance().getTime()));
		List<User> users = userRepository.findAll();
		for (User u: users) {
			cron(u);
			dailyTask.getUserDailyTasks(u);
		}
	}


	public void cron(User user) {
		try {
			habitica.cron(user.getApiUser(), user.getApiKey());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	private Message sendMessage(long chatId, String text, ReplyKeyboardMarkup replyKeyboardMarkup){
		SendMessage sendMessage = new SendMessage()
				.enableMarkdown(true)
				.setChatId(chatId)
				.setText(text)
				.setReplyMarkup(replyKeyboardMarkup);
		return execute(sendMessage);
	}



}
