package com.juliusdotsh.components;

import com.juliusdotsh.entities.User;

import java.util.Locale;
import java.util.ResourceBundle;

public class LanguageManager {
    public static String getLabel(User user, String label) {
        if (user == null || user.getLanguage() == null) {
            return ResourceBundle.getBundle("Message", new Locale("en")).getString(label);
        } else {
            return ResourceBundle.getBundle("Message", new Locale(user.getLanguage())).getString(label);
        }
    }
}
