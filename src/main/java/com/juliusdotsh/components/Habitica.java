package com.juliusdotsh.components;

import com.google.gson.Gson;
import com.juliusdotsh.entities.Task;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public
class Habitica {

	private HttpClient client = HttpClients.custom().setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD).build()).build();

	/***
	 This method runs getUserTask(https://habitica.com/apidoc/#api-Task-GetUserTasks)
	 in order to get the task of the day of the user specified
	 @param apiUser: the api user
	 @param apiKey: the api key
	 @throws Exception: when he has problem sending the request to Habitica
	 */
	public List<Task> getUserTask(String apiUser, String apiKey) throws Exception {
		HttpGet request = new HttpGet("https://habitica.com/api/v3/tasks/user");
		request.addHeader("x-api-user", apiUser);
		request.addHeader("x-api-key", apiKey);
		Gson gson = new Gson();
		String json = null;
		HttpResponse response = client.execute(request);
		if (response != null && response.getEntity() != null)
			json = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
		JSONArray jsonArray = new JSONObject(json).getJSONArray("data");
		Task[] result = gson.fromJson(jsonArray.toString(), Task[].class);
		request.releaseConnection();
		return Arrays.asList(result);
	}

	/***
		This method runs cron(https://habitica.com/apidoc/#api-Cron-Cron)
	 	this is used to fetch the daily tasks otherwise the api would give you the old tasks not completed or empty response
	 	@param apiUser: the api user
	 	@param apiKey: the api key
	 	@throws Exception: when he has problem sending the request to Habitica
	 */
	public void cron(String apiUser, String apiKey) throws Exception {
		HttpPost request = new HttpPost("https://habitica.com/api/v3/cron");
		request.addHeader("x-api-user",  apiUser);
		request.addHeader("x-api-key", apiKey);
		HttpResponse response = client.execute(request);
		request.releaseConnection();
	}
}
