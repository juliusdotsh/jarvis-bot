package com.juliusdotsh.components;

import com.juliusdotsh.commands.ApiKey;
import com.juliusdotsh.commands.ApiUser;
import com.juliusdotsh.commands.ChangeLanguage;
import com.juliusdotsh.commands.Start;
import com.juliusdotsh.entities.User;
import com.juliusdotsh.interfaces.Command;
import com.juliusdotsh.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Message;

@Component
public class StateHandler {

	private UserRepository userRepository;
	private CommandStrategy commandStrategy;
	private ApiUser apiUser;
	private ApiKey apiKey;
	private Start start;
	private ChangeLanguage changeLanguage;

	@Autowired
	public StateHandler(UserRepository userRepository, CommandStrategy commandStrategy, ApiKey apiKey, ApiUser apiUser, Start start, ChangeLanguage changeLanguage) {
		this.userRepository = userRepository;
		this.commandStrategy = commandStrategy;
		this.apiUser = apiUser;
		this.apiKey = apiKey;
		this.start = start;
		this.changeLanguage = changeLanguage;
	}

	public Command findCommandByState(Message message) {
		User user = userRepository.findByTelegramId(message.getChatId());
		Command command = null;
		if (user == null) {
			command = start;
		} else {
			switch (user.getState()) {
				case 0:
					command = commandStrategy.findCommandByMessageText(message.getText(), user);
					break;
				case 1:
					command = commandStrategy.checkForCancel(message.getText(), user);
					if (command == null) {
						command = apiUser;
					}
					break;
				case 2:
					command = commandStrategy.checkForCancel(message.getText(), user);
					if (command == null) {
						command = apiKey;
					}
					break;
				case 3:
					command = changeLanguage;
					break;
			}
		}
		return command;
	}
}
