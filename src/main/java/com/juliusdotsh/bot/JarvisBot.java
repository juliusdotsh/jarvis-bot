package com.juliusdotsh.bot;

import com.juliusdotsh.services.JarvisBotService;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;

import javax.annotation.PostConstruct;

@Controller
public class JarvisBot extends TelegramLongPollingBot {
	private Logger log = LoggerFactory.getLogger(JarvisBot.class);
	private JarvisBotService jarvisBotService;

	@Getter
	@Value("${jarvis.username}")
	private String botUsername;

	@Getter
	@Value("${jarvis.bottoken}")
	private String botToken;

	 @Autowired
	public JarvisBot(JarvisBotService jarvisBotService) {
		this.jarvisBotService = jarvisBotService;
	}


	@PostConstruct
	public void init(){
		try {
			TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
			telegramBotsApi.registerBot(this);
		} catch (TelegramApiRequestException e) {
			e.printStackTrace();
		}
	}

	public void onUpdateReceived(Update update) {
		Message message = update.getMessage();
	    if(message != null && message.hasText()) {
			jarvisBotService.onUpdateReceived(message);
	    }

	}

	@Override
	public void clearWebhook() throws TelegramApiRequestException {}

	@Scheduled(cron = "00 00 09 * * *")
	void cronDailyTasks(){
		jarvisBotService.cronDailyTasks();
	}
}
