DROP TABLE user;

CREATE TABLE user (
	id int not null AUTO_INCREMENT,
	telegram_id BIGINT,
	name varchar(255),
	api_user varchar(255),
	api_key varchar(255),
	state tinyint,
	cron BOOLEAN DEFAULT TRUE,
	constraint user_pk primary key(id),
	language varchar(2)
);