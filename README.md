Jarvis Bot
=====

Jarvis bot is a telegram bot that sends you notifications about the tasks you haven't completed on Habitica

Jarvis uses the SpringBoot framework and it's fully Dockerized

Quick start
-----------

### From dockerhub
1. Pull the image of Jarvis from dockerhub
https://hub.docker.com/r/juliusdotsh/jarvis-bot

2. Create a container on mysql with the database name jarvis ,  root password set to root123 and a custom network

3. Run the container from the image with the same network and configure the two enviroment variables JARVIS_BOTNAME and JARVIS_BOTTOKEN with the variables given from BotFather (Don't put the '@' character in the name of the bot)

4. Open your bot and Enjoy :D

### From source
1. Download the sources of the project

2. Launch maven with `clean package` goals

3. Launch docker compose with the two enviroment variables JARVIS_BOTNAME and JARVIS_BOTTOKEN with the variables given from BotFather (Don't put the '@' character in the name of the bot)

4. Open your bot and Enjoy :D